function FloodFill(){
  // Sets icon, display name, info tooltip and name of the tool
  this.name = "floodFill";
	this.icon = "assets/floodFill.jpg";
  this.display = "Fill Tool";
  this.infos = "This tool fills a space with a colour. Click on the space you want to fill.";

  // Declares variables
  var startX;
  var startY;

  this.draw = function() {
    undoAction();

    if (mousePressOnCanvas(c)&&mouseIsPressed) {
      startX = mouseX;
      startY = mouseY;
      flood(startX,startY);
    }
  }


  function flood(x,y){
    // Must begin with this
    // Load all the pixels currently on canvas into the pixels array
    loadPixels();

    // ColourP uses colour names for some strange reason
    // so this converts a colour name into an object containing rgb values
    col = color(colourP.selectedColour);

    // put rgb values in indiviual variables
    let pixelClicked = getPixel(x,y);
    let r = pixelClicked.red;
    let g = pixelClicked.green;
    let b = pixelClicked.blue;

    // array representing the area to be filled
    // conceptualised as an island of true surrounded by a sea of false
    let homeIsland = [];
    // array representing entire pixel grid as
    // true = same as desired colour; false = different to desired colour
    let fillArr = [];
    // array representing pixels that were just changed to match desired colour
    let justChArr = [];

    let d = pixelDensity();


    for(let i = 0; i < height; i++){
      fillArr.push([]);
      for(let j = 0; j < width; j++){
        // get a pixel

        // loop over
        let index = 4 * ((i * d) * width * d + (j * d));
        let rPx = pixels[index];
        let gPx = pixels[index+1];
        let bPx = pixels[index+2];

        // the magic happens here
        // iterate through entire pixel grid, abstracting it to true or false
        // representation of right vs wrong colour (does it match r, g, b)
        if((rPx===r) && (gPx===g) && (bPx===b)){
          fillArr[i].push(true);
        } else {
          fillArr[i].push(false);
        }
      }
    }

    homeIsland.push([x,y]);
    justChArr.push([x,y]);
    fillArr[y][x] = false;

    while (justChArr.length > 0) {

      let coords = justChArr[0];
      let xCoord = coords[0];
      let yCoord = coords[1];

      // if statement to check pixel to left
      if (xCoord>0) {
        if ((fillArr[yCoord][xCoord-1] === true)) {
          fillArr[yCoord][xCoord-1] = false;
          homeIsland.push([xCoord-1,yCoord]);
          justChArr.push([xCoord-1,yCoord]);
        }
      }

      // if statement to check pixel to right
      if (xCoord+1<fillArr[yCoord].length) {
        if ((fillArr[yCoord][xCoord+1] === true)) {
          fillArr[yCoord][xCoord+1] = false;
          homeIsland.push([xCoord+1,yCoord]);
          justChArr.push([xCoord+1,yCoord]);
        }
      }

      // if statement to check pixel to top
      if (yCoord>0) {
        if ((fillArr[yCoord-1][xCoord] === true)) {
          fillArr[yCoord-1][xCoord] = false;
          homeIsland.push([xCoord,yCoord-1]);
          justChArr.push([xCoord,yCoord-1]);
        }
      }

      // if statement to check pixel to bottom
      if (yCoord+1<fillArr.length) {
        if ((fillArr[yCoord+1][xCoord] === true)) {
          fillArr[yCoord+1][xCoord] = false;
          homeIsland.push([xCoord,yCoord+1]);
          justChArr.push([xCoord,yCoord+1]);
        }
      }


      // remove entry in justChArr
      justChArr.splice(0,1);
    }

    // fill in the area with the new colour
    let m = homeIsland.length
    for(let i = 0; i < m; i++){
      let x = homeIsland[i][0];
      let y = homeIsland[i][1];
      let r = col.levels[0];
      let g = col.levels[1];
      let b = col.levels[2];
      setPixel(x,y,r,g,b);
    }

    // must end with this
    // update the pixels on canvas to match the pixels array
    updatePixels();
  }
  this.populateOptions = function() {

	}
	this.unselectTool = function() {

	}
}
