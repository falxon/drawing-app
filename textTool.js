function TextTool() {
	// Sets icon, display name, info tooltip and name of the tool
	this.name = "textTool";
	this.icon = "assets/textTool.jpg";
	this.display = "Text Tool";
	this.infos = "This tool places text on the canvas. You can customise it using the options, and click on the canvas when you are ready. Click to move the text, and click finish when you are done.";


	// Declares and assigns variables
	var self = this;
	var fillButton;
	var textS;
	var textInput;
	var textContent;
	var placed = false;
	var previousMouseX;
	var previousMouseY;
	var textFontInput;
	var fonts = [];

	// Constructor for font object
	function Font(fontData){
		this.name = fontData.name;
		this.value = fontData.value;
		this.file = fontData.file;
		this.font = false;
		this.callback = function(font){
			this.font = font;
			textFont(font);
		}
	}
	// Loads font data from external file
	// Constructs new font object and pushes to array
	loadJSON("json-data/textToolFonts.json", function(fontData){
		let l = fontData.length;
		for (let i = 0; i < l; i++) {
			let font = new Font(fontData[i]);
			fonts.push(font);
		}
	});

	loadPixels();
	textAlign(CENTER, CENTER);


	this.draw = function() {
		// Listener for text input
		textInput = select("#text-input");
		textInput.input(function(){
			textContent = this.value();
		});
		textSize(textS);



		// Display the last save state of pixels
		updatePixels();

		//do the drawing if the mouse is pressed
		if (mouseIsPressed&&mousePressOnCanvas(c)) {
			previousMouseX = mouseX;
			previousMouseY = mouseY;
			if (placed === false) {
				loadPixels();
				// Update undo stack.
				updateUndoStack();
				placed = true;
			}
		}
		if (!(previousMouseX===undefined&&previousMouseY===undefined)) {
			transformationsDraw(previousMouseX,previousMouseY);
			text(textContent,0,0);
		}
	};

	//when the tool is deselected update the pixels
	this.unselectTool = function() {
		loadPixels();
		rot = 0;
		stretchX = 1;
		stretchY = 1;
		previousMouseX = undefined;
		previousMouseY = undefined;
		placed = false;
		textContent = undefined;
	};


	this.populateOptions = function() {
		// Create options menu
		// Create div for text size module
		let textSizeDiv = createDiv("<h4>Text Size</h4>");
		textSizeDiv.parent("left-options");
		textSizeDiv.id("text-size-div");
		textSizeDiv.class("option-block");
		// Create input for module
		let textSizeInput = createInput("20","number");
		textSizeInput.id("text-size");
		textSizeInput.parent("text-size-div");
		// Event listener gets value
		textSizeInput.input(function(){
			textS = Number(this.value());
			textSize(textS);
		});
		// Create div for font select menu
		let fontSelectDiv = createDiv("<h4>Font</h4>");
		fontSelectDiv.id("font-select-div");
		fontSelectDiv.class("option-block");
		fontSelectDiv.parent("left-options");
		// Create select and options for select menu
		let fontSelect = createSelect();
		fontSelect.parent("font-select-div");
		fontSelect.id("font-select");
		// Creates options from fonts array
		let l = fonts.length;
		for (let i = 0; i < l; i++) {
			fontSelect.option(fonts[i].name,fonts[i].value);
		}
		// Event listener to read user input
		fontSelect.changed(function(){
			font = this.value();
			let l = fonts.length;
			for (let i = 0; i < l; i++) {
				if (font === fonts[i].value) {
					if (fonts[i].font === false) {
						loadFont(fonts[i].file,fonts[i].callback);
					} else {
						textFont(fonts[i].font);
					}
					return;
				}
			}
		});
		// Create div for text input
		let textInputDiv = createDiv("<h4>Text</h4>");
		textInputDiv.parent("right-options");
		textInputDiv.id("text-input-div");
		textInputDiv.class("option-block");
		// Create paragraph
		let textInputP = createP("Type here");
		textInputP.parent("text-input-div");
		// Create input for module
		let textInput = createInput("","text");
		textInput.parent("text-input-div");
		textInput.id("text-input");
		// Create div for buttons
		let buttonDiv = createDiv();
		buttonDiv.parent("right-options");
		buttonDiv.id("button-div");
		let finishButton = createButton("Finish Text");
		finishButton.id("finish-text");
		finishButton.parent("button-div");
		finishButton.mousePressed(function(){
			// Resets variables and loads pixels
			loadPixels();
			previousMouseX = undefined;
			previousMouseY = undefined;
			placed = false;
		})
		// Places transformations modules
		transformations();

	};
}
