// Store previous states for undo button in stack now stack is availabile
previousState = new Stack;

function HelperFunctions() {

	//p5.dom click click events. Notice that there is no this. at the
	//start we don't need to do that here because the event will
	//be added to the button and doesn't 'belong' to the object

	// Event handler for the clear button event. Clears the screen
	select("#clearButton").mouseClicked(function() {

		// Create dialog box to ask for user confirmation
		if (confirm("Are you sure you want to clear the canvas?")) {
			// Clear background by setting it to white
			background("#FFFFFF");
			// Call loadPixels to update the drawing state
			// This is needed for the mirror tool
			loadPixels();
		}

	});

	// Event handler for the save image button
	// Saves the canvas to the local file system.
	select("#saveImageButton").mouseClicked(function() {

    // Ask the user what they want to call their file and store the answer
		// in a variable
    var fileName = prompt("What would you like to call your file?");

		// Save the current canvas to disk
        saveCanvas(fileName, "png");
	});

	select("#undoButton").mouseClicked(function() {
		// Loads canvas into pixels
		loadPixels();

		// Check if action is possible
		if(previousState.peek() !== null){
			// Changes pixels array to the state saved in previousState
			// before the last action
			for (var i = 0; i < pixels.length; i++) {
				pixels[i] = previousState.peek()[i];
			}
			// Pop the stack so the program is ready to go back another step
			previousState.pop();
		}

		// Saves changes to the canvas
		updatePixels();

	});

}

function mousePressOnCanvas(canvas) {
	// Checks for google chrome as different code must be run
	if (navigator.vendor !== "Google Inc.") {
		// Do not accept clicks outside the canvas
		// using appropriate offset calculation for chrome
		if ((mouseX > 0) &&
				(mouseX < canvas.width) &&
				(mouseY > 0) &&
				(mouseY < canvas.height)) {
			// Returns true if click was legal
			return true;
		}
	} else {
		if ((mouseX > canvas.elt.offsetLeft) &&
				(mouseX < (canvas.elt.offsetLeft + canvas.width)) &&
				(mouseY > canvas.elt.offsetTop) &&
				(mouseY < (canvas.elt.offsetTop + canvas.height))) {
			// Returns true if click was legal
			return true;
		}
	}
	// Returns false if click was not permitted
	return false;
}

// Gets and returns the colour of a given pixel
function getPixel(x,y){
	// Accounts for differences in pixel density caused by retina and smiliar
	// screens
	let d = pixelDensity();
	// declares object to contain data. Alpha channel is not necessary as
	// alpha has not been used.
	let cols = {
		red: 0,
		green: 0,
		blue: 0
	}
	// Calculates position in pixels[] using formula.
	// High pixel density screens here have multiple pixels storing
	// the same colour data
	// It is not necessary to read all of them, just one
	let index = 4 * ((y * d) * width * d + (x * d));
	// Assigns red, green and blue values to appropriate properties
	cols.red = pixels[index];
	cols.green = pixels[index+1];
	cols.blue = pixels[index+2];

	// Returns data
	return cols;
}

// Sets the colour of a pixel in pixels[]
function setPixel(x,y,r,g,b){
	// Accounts for differences in pixel density caused by
	// retina and smiliar screens
	let d = pixelDensity();

	// Iterates through additional pixels if the screen is high pixel density
	// Only runs once on lower density screens
	for (let i = 0; i < d; i++) {
		for (let j = 0; j < d; j++) {
			// Calculates position in pixels[] using formula
			index = 4 * ((y * d + j) * width * d + (x * d + i));
			// Sets pixels at the required indeces to the colours
			// passed to the function
			pixels[index] = r;
			pixels[index+1] = g;
			pixels[index+2] = b;
		}
	}
}

// Provides common options for every tool,
// which rely on globals to provide global settings indicators
// Requires statesUpdate() running in draw in order to function correctly
function states(infos){
	// Create fill toggle module
	// Create div for module
	let fillRDiv = createDiv("<h4>Fill Shapes</h4>");
	fillRDiv.parent("middle-1-options");
	fillRDiv.id("fill-toggle-div");
	fillRDiv.class("option-block");

	// Create radio with options
	let fillR = createRadio();
	fillR.id("fill-toggle");
	fillR.parent("fill-toggle-div");
	fillR.option("On", "true");
	fillR.option("Off", "false");
	// Event listener to check for changes to setting
	fillR.changed(function(){
		// Changes global variable based on toggle position
		if (fillR.value()=="true") {
			fillSt = true;
		} else if (fillR.value()=="false") {
			fillSt = false;
		}
	});
	// Create fill toggle module
	// Create div for module
	let strokeWeightDiv = createDiv();
	strokeWeightDiv.parent("middle-1-options");
	strokeWeightDiv.id("stroke-weight-div")
	strokeWeightDiv.class("option-block");
	// Add title to module
	let strokeWeightTitle = createElement("h4","Stroke Weight");
	strokeWeightTitle.parent("stroke-weight-div");
	// Create input
	let strokeWeightInput = createInput(strokeW, "number");
	strokeWeightInput.parent("stroke-weight-div");
	strokeWeightInput.id("stroke-weight");
	// Event listener to check for input
	strokeWeightInput.input(function(){
		// Assigns input value to a global
		strokeW = this.value();
		//If input is not a legal stroke weight, change to 1
		if (strokeW<1) {
			strokeW = 1;
		}
		strokeWeight(strokeW);
	})
	// Create info module
	let infoDiv = createDiv("Hover for information");
	infoDiv.parent("bottom-options");
	// Adds title attribute to div and gives it the value passed to the function
	infoDiv.attribute("title", infos);
	infoDiv.id("info-hover");
}

// Updates global states
function statesUpdate(){
	// Selects fill radio by ID
	let fillR = select("#fill-toggle");
	// Monitors global fill state variable
	// Changes settings appropriately
	// Adjusts interface based on internal state
	if (fillSt===true) {
		fill(colourP.selectedColour);
		stroke(colourP.selectedColour);
		fillR.value("true");
	}else if (fillSt===false) {
		noFill();
		stroke(colourP.selectedColour);
		fillR.value("false");
	}
	// Selects stroke weight input by ID
	let strokeWeightInput = select("#stroke-weight");
	// Checks if the value from the input is different to the global
	// and changes input value if it is
	if (!(strokeWeightInput.value()==strokeW)) {
		strokeWeightInput.value(strokeW);
	}
}

// Sets fill state and changes global variable to match
function setFill(colour){
	fillSt = true;
	fill(colour);
}
function unsetFill(){
	fillSt = false;
	noFill();
}

// Clears options for tool unselection
// Must preserve column structure
function clearOptions(){
	select(".left").html("");
	select(".middle-1").html("");
	select(".middle-2").html("");
	select(".right").html("");
	select(".bottom").html("");
}

// Creates modules for commonly used transformation options
function transformations(){
	// Create stretch module
	// Create div for module
	let stretchInputDiv = createDiv("<h4>Stretch</h4>");
	stretchInputDiv.parent("middle-2-options");
	stretchInputDiv.id("stretch-input-div");
	stretchInputDiv.class("option-block");
	// Create input and label for X stretch
	let stretchInputXLabel = createElement("label","x: ");
	stretchInputXLabel.attribute("for","stretch-input-x");
	stretchInputXLabel.parent("stretch-input-div");
	let stretchInputX = createInput("1","number");
	stretchInputX.parent("stretch-input-div");
	stretchInputX.id("stretch-input-x");
	// Event listener to monitor field
	stretchInputX.input(function(){
		// Assigns value of field to variable
		stretchX = this.value();
	});
	// Create input and label for Y stretch
	let stretchInputYLabel = createElement("label","y: ");
	stretchInputYLabel.attribute("for","stretch-input-y");
	stretchInputYLabel.parent("stretch-input-div");
	let stretchInputY = createInput("1","number");
	stretchInputY.parent("stretch-input-div");
	stretchInputY.id("stretch-input-y");
	// Event listener to monitor field
	stretchInputY.input(function(){
		// Assigns value of field to variable
		stretchY = this.value();
	});

	// Create rotation module
	// Create div for module
	let rotateInputDiv = createDiv("<h4>Rotation</h4>");
	rotateInputDiv.parent("middle-2-options");
	rotateInputDiv.id("rotate-input-div");
	rotateInputDiv.class("option-block");
	// Create input for module
	let rotateInput = createInput("0","number");
	rotateInput.parent("rotate-input-div");
	rotateInput.id("rotate-input");
	// Event listener to monitor field
	rotateInput.input(function(){
		// Assigns value of field to variable
		rot = this.value();
	});
}

// Sets state of transformation effects before drawing to canvas
function transformationsDraw(x,y){
	// Sets angleMode to degrees as most users will prefer to input in
	// degrees and this means that no conversion is necessary
	angleMode(DEGREES);
	// Moves origin (0,0) to coordinates passed to function
	translate(x, y);
	// Applies rotation
	rotate(rot);
	// Applies stretch matrix to drawing
	// If stretchX and stretchY are 1, then no stretching occurs
	// If they are the same, then scale is maintained
	// A value in a position stretches the shape by that factor, along that axis
	applyMatrix(stretchX,0,0,stretchY,0,0);
}

// Saves state of canvas
// Only works for tools  which use one click per action
function undoAction(){
	// If there is a click on the canvas, save canvas state
	// before changes are applied
	if ((mouseIsPressed&&mousePressOnCanvas(c))&&clicked === false) {
		// Load canvas into pixels[]
		loadPixels();
		updateUndoStack();
		//console.log(previousState);
		// set clicked to true so code is not run again until the next action
		clicked = true;
	}
	// When action is finished, set clicked to false so that state
	// can be stored again if another action is performed
	if (clicked === true && !mouseIsPressed) {
		clicked = false;
	}
}

function updateUndoStack(){
	// Push most recent state of pixels onto stack
	previousState.push(pixels.slice(0, pixels.length));
}

function Element(value) {
	this.value = value;
	this.next = null;
}

// Stack constructor function
function Stack() {

	this.top = null;

	this.push = function(o) {
		var element = new Element(o);
		element.next = this.top;
		this.top = element;
	};

	this.peek = function() {
		if (this.top === null) {
			return null;
		}
		return this.top.value;
	};

	this.pop = function() {
		if (this.top === null) {
			return "Stack underflow!";
		}
		this.top = this.top.next;
	};

	this.isEmpty = function() {
			return (this.top === null);
	};
}
