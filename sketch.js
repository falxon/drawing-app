// Global variables that will store the toolbox colour palette
// and the helper functions.
var toolbox = null;
var colourP = null;
var helpers = null;
var fillSt = true;
var strokeW = 1;
var infos;
var c;
var rot = 0;
var stretchX = 1;
var stretchY = 1;
var previousState; // Will contain a stack for undo feature.
let clicked = false;
let undo = false;

function setup() {

	//create a canvas to fill the content div from index.html
	canvasContainer = select('#content');
	c = createCanvas(canvasContainer.size().width, canvasContainer.size().height);
	c.parent("content");

	//create helper functions and the colour palette
  helpers = new HelperFunctions();
	colourP = new ColourPalette();

	//create a toolbox for storing the tools
	toolbox = new Toolbox();

	//add the tools to the toolbox.
	toolbox.addTool(new FreehandTool());
	toolbox.addTool(new LineToTool());
	toolbox.addTool(new SprayCanTool());
	toolbox.addTool(new PaintbrushTool()); // added paintbrush tool to toolbox
	toolbox.addTool(new MirrorDrawTool()); // was renamed for consistency
  toolbox.addTool(new RectangleTool()); // added rectangle tool feature
	toolbox.addTool(new EllipseTool()); // added ellipse tool feature
	toolbox.addTool(new ShapeTool()); // added shape tool feature
	toolbox.addTool(new TextTool()); // added text tool feature
	toolbox.addTool(new FloodFill()); // added flood fill feature
	toolbox.addTool(new ImageTool()); // added insert image feature

	//toolbox.addTool(new BlurTool()); // added blur tool

	background(255);
}

function draw() {
	statesUpdate();
	//call the draw function from the selected tool.
	//hasOwnProperty is a javascript function that tests
	//if an object contains a particular method or property
	//if there isn't a draw method the app will alert the user
	if (toolbox.selectedTool.hasOwnProperty("draw")) {
		toolbox.selectedTool.draw();
	} else {
		alert("it doesn't look like your tool has a draw method!");
	}
}
