function ShapeTool(){
	// Sets icon, display name, info tooltip and name of the tool
	this.icon = "assets/shape.jpg";
	this.name = "Shape";
	this.display = "Shape Tool";
	this.infos = "This tool draws shapes. Click to set a corner, or drag to draw a curved line. Click on a corner to move, and click finish shape when done.";

	// Declares and assigns variables
	var editButton;
	var finishButton;
	var editState = false;
	var currentShape = [];
	var begunShape = false;

	// Loads canvas into pixels[]
	loadPixels();


	this.draw = function(){
		// Update canvas with current changes
		updatePixels();
		// If the mouse button is pressed
		if(mousePressOnCanvas(c)&&mouseIsPressed){
			// Undo stack handling
			if (begunShape===false) {
				loadPixels();
				updateUndoStack();
				begunShape = true;
			}
			// If editState is false
			if (!editState) {
				// Push coordinates to currentShape
				currentShape.push({
					x: mouseX,
					y: mouseY
				});
			}
			// If editState is true
			else {
				// Iterate through currentShape checking for point interaction

				for (let i = 0; i < l; i++) {
					if (dist(currentShape[i].x,currentShape[i].y,mouseX,mouseY) < 7) {
						currentShape[i].x = mouseX;
						currentShape[i].y = mouseY;
					}
				}
			}
		}
		// Draw currentShape
		beginShape();
		let l = currentShape.length;
		for (let i = 0; i < currentShape.length; i++) {
			vertex(currentShape[i].x,currentShape[i].y);
		}
		endShape();

	};

	this.populateOptions = function() {

		// Create options menu
		editButton = createButton("Edit Shape");
		editButton.parent("left-options");
		editButton.mousePressed(function(){
			if (editState) {
				editState = false;
				editButton.html("Edit Shape");
			} else {
				editState = true;
				editButton.html("Create Vertices");
			}
		})
		finishButton = createButton("Finish Shape");
		finishButton.parent("left-options");
		finishButton.mousePressed(function(){
			// Reset variables and load current picel data
			loadPixels();
			currentShape = [];
			begunShape = false;
			editState = false;
			editButton.html("Edit Shape");
		})

	};

	this.unselectTool = function() {
		// Reset variables and load current picel data
		loadPixels();
		begunShape = false;
		editState = false;
		currentShape = [];
	}
}
