// Spray can constructor function
function SprayCanTool() {
  // Sets icon, display name, info tooltip and name of the tool
    this.name = "sprayCanTool";
    this.icon = "assets/sprayCan.jpg";
    this.display = "Spray Can Tool";
    this.infos = "This tool is a spray can. Click and drag on the canvas.";
    this.points = 13;
    this.spread = 10;

    this.draw = function(){
    undoAction();
    //if the mouse is pressed paint on the canvas
    //spread describes how far to spread the paint from the mouse pointer
    //points holds how many pixels of paint for each mouse press.
    if(mouseIsPressed){
        for(let i = 0; i < this.points; i++){
            point(random(mouseX-this.spread, mouseX + this.spread),
                random(mouseY-this.spread, mouseY+this.spread));
        }
      }
    }
    this.populateOptions = function() {

  	}
  	this.unselectTool = function() {
  	}
};
