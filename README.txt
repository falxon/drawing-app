Draw app

This is an app which allows the user to draw digitally. It combines pixel and vector art. It uses p5 and javascript.

Features implemented:
- Rectangle tool
- Ellipse tool
- Vertex tool
- Text tool

under construction:
- flood fill tool
- make it possible to change stroke width and fill state with all tools

future:
- eraser tool
- load tool
- UI redesign

We as yet haven't been able to solve a firefox bug which causes things to be drawn off canvas sometimes. We can fix it by detecting which browser is in use and having some custom code.
