// Declares function PaintbrushTool
function PaintbrushTool(){
	// Sets icon, display name, info tooltip and name of the tool
	this.icon = "assets/paintbrush.jpg";
	this.name = "Paintbrush";
	this.display = "Paintbrush Tool";
	this.infos = "This tool draws lines using different brushes. Click on canvas to stamp the shape and drag to draw. Use the settings to adjust the brush, or choose a selection from the presets";


	// Declares and assigns variables
	var size = 10;
	var shape = "circle";
	var sizeLabel;



	this.draw = function(){
		undoAction();
		// If the mouse button is pressed
		if(mouseIsPressed&&mousePressOnCanvas(c)){
			// If statement to detect shapes
			// Circle is default
			// Applies transformations and draws shape
			if (shape === "circle") {
				transformationsDraw(mouseX,mouseY);
				ellipse(0,0,size);
			} else if (shape === "square") {
				transformationsDraw(mouseX,mouseY);
				rectMode(CENTER);
				rect(0,0,size,size);
			} else if (shape === "triangle") {
				transformationsDraw(mouseX,mouseY);
				let tHeight = sq(size)-sq(size/2);
				tHeight = sqrt(tHeight);
				triangle(0, 0-tHeight/2,0-size/2, 0+tHeight/2,0+size/2, 0+tHeight/2);
			}


		}
	};
	this.populateOptions = function() {
		// Create module for shape input
		// Create div for module
		let shapeInputDiv = createDiv("<h4>Shape</h4>");
		shapeInputDiv.parent("left-options");
		shapeInputDiv.id("shape-input-div");
		shapeInputDiv.class("option-block");
		// Create input and add options
		let shapeInput = createRadio();
		shapeInput.id("shape-div");
		shapeInput.parent("shape-input-div");
		shapeInput.option("circle");
		shapeInput.option("square");
		shapeInput.option("triangle");
		shapeInput.value("circle");
		// Event listener assigns input to variable
		shapeInput.changed(function(){
			shape = shapeInput.value();
		});
		// Create size input module
		// Create div for module
		let sizeInputDiv = createDiv("<h4>Size</h4>");
		sizeInputDiv.parent("left-options");
		sizeInputDiv.id("size-input-div");
		sizeInputDiv.class("option-block");
		let sizeDiv = createDiv();
		sizeDiv.id("size-div");
		sizeDiv.parent("size-input-div");
		sizeInput = createInput(size, "number");
		sizeInput.parent("size-div");
		sizeInput.id("size");
		// Event listener assigns input to variable
		sizeInput.input(function(){
			size = this.value();
		});
		// Create presets module
		// Create div for module
		let presetsDiv = createDiv("<h4>Presets</h4>");
		presetsDiv.parent("right-options");
		presetsDiv.class("option-block");
		presetsDiv.id("presets-div");
		// Create select menu
		let presetsSelect = createSelect();
		presetsSelect.id("presets-select");
		presetsSelect.parent("presets-div");
		// Load presets using JSON
		loadJSON("json-data/paintbrushPresets.json", function(presets){
			// Once JSON is loaded...
			// Populate presets menu
			let l = presets.length;
			for (let i = 0; i < l; i++) {
				presetsSelect.option(presets[i].name);
			}
			// Add event listener to see when the user has chosen a preset
			presetsSelect.changed(function(){
				for (let i = 0; i < l; i++) {
					if (presets[i].name===this.value()) {
						let sizeInput = select("#size");
						sizeInput.value(presets[i].size);
						size = presets[i].size;

						shape = presets[i].shape;
						let shapeInput = select("#shape-div");
						shapeInput.value(presets[i].shape);

						rot = presets[i].rotation;
						let rotInput = select("#rotate-input");
						rotInput.value(presets[i].rotation);

						strokeW = presets[i].stroke;
						let strokeWeightInput = select("#stroke-weight");
						strokeWeightInput.value(presets[i].stroke);
						strokeWeight(strokeW);

						fillSt = presets[i].fill;

						stretchX = presets[i].stretchX;
						let stretchXInput = select("#stretch-input-x");
						stretchXInput.value(presets[i].stretchX);

						stretchY = presets[i].stretchY;
						let stretchYInput = select("#stretch-input-y");
						stretchYInput.value(presets[i].stretchY);
					}
				}
			});
		});

		transformations();
	}
	this.unselectTool = function() {
		// Sets transformation values to default
		rot = 0;
		stretchX = 1;
		stretchY = 1;
	}
}
