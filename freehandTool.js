function FreehandTool(){
	// Sets icon, display name, info tooltip and name of the tool
	this.icon = "assets/freehand.jpg";
	this.name = "freehand";
	this.display = "Freehand Draw Tool";
	this.infos = "This tool draws a curved line. Click and drag on the canvas.";

	// To smoothly draw we'll draw a line from the previous mouse location
	// to the current mouse location. The following values store
	// the locations from the last frame. They are -1 to start with because
	// we haven't started drawing yet.
	var previousMouseX = -1;
	var previousMouseY = -1;


	this.draw = function(){
		undoAction();
		// If the mouse is pressed
		if(mouseIsPressed){
			// Check if they previousX and Y are -1. set them to the current
			// mouse X and Y if they are.
			if (previousMouseX == -1){
				previousMouseX = mouseX;
				previousMouseY = mouseY;
			}
			// If we already have values for previousX and Y we can draw a line from
			// there to the current mouse location
			else{
				line(previousMouseX, previousMouseY, mouseX, mouseY);
				previousMouseX = mouseX;
				previousMouseY = mouseY;
			}
		}
		// If the user has released the mouse we want to set the previousMouse values
		// back to -1.
		// Try and comment out these lines and see what happens!
		else{
			previousMouseX = -1;
			previousMouseY = -1;
		}
	};
	this.populateOptions = function() {
	}
	this.unselectTool = function() {
	}
}
