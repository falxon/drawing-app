// Declares function EllipseTool
function EllipseTool(){
	// Sets icon, display name, info tooltip and name of the tool
	this.icon = "assets/ellipse.jpg";
	this.name = "Ellipse";
	this.display = "Ellipse Tool";
	this.infos = "This tool draws an ellipse. Click and drag on canvas.";

	// Declares and assigns variables
	var startMouseX = -1;
	var startMouseY = -1;
	var drawing = false;



	this.draw = function(){
		undoAction();
		// If the mouse button is pressed
		if(mouseIsPressed&&mousePressOnCanvas(c)){
			if(startMouseX == -1){
				// If startMouseX is equal to -1, set startMouseX and startMouseY
				// to the values of mouseX and mouseY
				startMouseX = mouseX;
				startMouseY = mouseY;
				// Set drawing equal to true
				drawing = true;
				// Loads the data for the display into an array called pixels[]
				loadPixels();
			}

			else{
				// Applies changes made to pixel data
				updatePixels();
				// Create an ellipse with the starting corner at the start point
        // and the opposite corner at the point the user releases the mouse
				ellipseMode(CORNERS);
				ellipse(startMouseX,startMouseY,mouseX,mouseY);
			}

		}
		// If mousebutton is not pressed and drawing equals true
		// sets drawing to false and start-points to -1
		else if(drawing){
			drawing = false;
			startMouseX = -1;
			startMouseY = -1;
		}
	};

	this.populateOptions = function() {
	}
	this.unselectTool = function() {
	}
}
