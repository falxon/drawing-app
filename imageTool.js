function ImageTool() {
	// Sets icon, display name, info tooltip and name of the tool
	this.name = "image";
	this.icon = "assets/image.jpg";
	this.display = "Image Tool";
	this.infos =
	`This tool imports an image into the program, and lets you reposition and resize the image before saving it. Choose an image from your computer by clicking browse, then click the canvas to place it. Use the centre handle to move the image; use the corner handles to resize it. Click the finish button to stop editing the image.`;

	let p5image; // Native p5 image object goes here
	let currentImg = {}; // Position and size variable for image
	let oldMouseX; // Position of mouse on previous frame
	let oldMouseY;
	let oldImgWidth; // Size of image on previous frame
	let oldImgHeight;
	let oldImgX; // Position of image on previous frame
	let oldImgY;
	let moveX; // Amount image should be moved by according to user interaction
	let moveY;
	let resizeX = 0; // Amount image should resize according to user interaction
	let resizeY = 0;
	let previewImg;

	// Program state flag variables
	let newImgLoaded = false;
	let placeMode = false;
	let imgDownscaled = false;
	let resizingNow = false;
	let movingNow = false;

	loadPixels();

	this.draw = function() {
		const box = 15; // Default size for img resizing/moving handles

		updatePixels();

		// If user tries to place image on the canvas
		if((mouseIsPressed && newImgLoaded === true) && mousePressOnCanvas(c)){
			// Ensure the following code only runs once
			newImgLoaded = false;

			// Put all variables needed for resizing image in one convenient place
			currentImg = {
											data: p5image,
											x: mouseX,
											y: mouseY,
											width: p5image.canvas.width,
											height: p5image.canvas.height
			}

			// Ensure image will be out in the right place on canvas
			downScaleImage();
			centreImage();

			// Set flag to enable place mode which allows the user to resize and move
			// the image on the canvas as they please
			placeMode = true;
			loadPixels();
			// Save state of canvas pre-image-placement
			// This is needed for the undo button to work
			updateUndoStack();
		}

		// Check to see if place mode is enabled
		if(placeMode === true){
			// Remove any ghosts of previous image positioning
			loadPixels();

			// Place image on canvas ready for user manipulation
			image(currentImg.data,
						currentImg.x,
						currentImg.y,
						currentImg.width,
						currentImg.height);

			push();

			// set colours to draw resize and movement handles
			strokeWeight(2);
			stroke(0);
			fill(255);

			// These functions allow the user to resize and move the image
			moveImage();
			resizeImage();

			pop(); // Feset drawing state for user to draw with other tools
		}

		function downScaleImage(){
			// Make sure image fits on canvas, downscaling it if it doesn't
			let aspectRatio = currentImg.height / currentImg.width;
			if(imgDownscaled === false){
				if(currentImg.width > width / 3 * 2){
					currentImg.width = width / 3 * 2;
					currentImg.height = currentImg.width * aspectRatio;
				}
				if(currentImg.height > height / 3 * 2){
					currentImg.height = height / 3 * 2;
					currentImg.width = currentImg.height / aspectRatio;
				}
				imgDownscaled = true;
			}
		}

		function centreImage(){
			let imageCentre = [currentImg.x + currentImg.width/2,
												currentImg.y + currentImg.height/2];

			// If the image's centre handle has appeared off screen
			if (imageCentre[0] > width - 20 || imageCentre[1] > height - 20){
				// Place image in the middle of the canvas
				currentImg.x = width/2 - currentImg.width/2;
				currentImg.y = height/2 - currentImg.height/2;
			}
		}

		function moveImage(){
			// Define position of centre handle for moving image
			let centreBox = [
				currentImg.x + currentImg.width/2 - box/2,
				currentImg.y + currentImg.height/2 - box/2,
				box,
				box
			];
			// Generate centre handle for moving the image
			rect(centreBox[0], centreBox[1], centreBox[2], centreBox[3]);

			// Image movement code...
			// Check if the mouse is inside each move handle and pressed
			if(((mouseX>=centreBox[0] && mouseX<=centreBox[0]+box) &&
					(mouseY>=centreBox[1] && mouseY<=centreBox[1]+box)) ||
					movingNow === true) {
				if(mouseIsPressed === true){
					// Latch the movement on until user releases mouse
					if(movingNow === false){
						// This code runs once per user interaction with move handles
						movingNow = true;
						// Set up oldMouse and oldImg variables for this movement
						// interaction
						oldMouseX = mouseX;
						oldMouseY = mouseY;
						oldImgX = currentImg.x;
						oldImgY = currentImg.y;
					} else {
						// Set values of move variable by comparing current mouse position
						// with mouse position last frame
						moveX = mouseX - oldMouseX;
						moveY = mouseY - oldMouseY;
						// Move image by appropriate amount
						currentImg.x = oldImgX + moveX;
						currentImg.y = oldImgY + moveY;
					}
				} else {
					movingNow = false;
				}
			}
		}

		function resizeImage(){
			// Define positions of corner handles in relation to the image
			// Corner handles will allow the user to resize the image
			let corners = [
				[currentImg.x, currentImg.y, box, box],
				[currentImg.x+currentImg.width-box/2, currentImg.y, box, box],
				[currentImg.x, currentImg.y+currentImg.height-box/2, box, box],
				[currentImg.x+currentImg.width-box/2,
					currentImg.y+currentImg.height-box/2, box, box]
			];

			// Generate corner rectangles for resizing of image
			for(let i = 0; i < 4; i++) {
				// Prevent the resize boxes being drawn as the image is saved to canvas
				rect(corners[i][0], corners[i][1], corners[i][2], corners[i][3]);

				// Image resize code...
				// Check if the mouse is inside any of the corners or if resizing has
				// already been initiated. The resizingNow check stops the resizing
				// being interrupted when the movement of the corner-squares can't
				// keep up with the mouse pointer.
				if(((mouseX>=corners[i][0] && mouseX<=corners[i][0]+box) &&
						(mouseY>=corners[i][1] && mouseY<=corners[i][1]+box)) ||
						resizingNow === true){
					// Latch resizing on until user releases mouse
					if(mouseIsPressed === true){
						if(resizingNow === false){
							// This code runs once per resize interaction from the user
							resizingNow = true;
							oldMouseX = mouseX;
							oldMouseY = mouseY;
							oldImgWidth = currentImg.width;
							oldImgHeight = currentImg.height;
						} else {
							// Set resize variables to the amounts the mouse moved last frame
							resizeX = mouseX - oldMouseX;
							resizeY = mouseY - oldMouseY;
							// Update size of image based on resize variables
							currentImg.width = oldImgWidth + resizeX;
							currentImg.height = oldImgHeight + resizeY;
						}
					} else {
						// If the mouse is released, stop resizing
						resizingNow = false;
					}
				}
			}
		}
	}

	// Embed image in canvas and reset tool so another may be added if wanted
	function placeImage() {
		updatePixels();
		// Draw image to canvas in final position without handles
		image(currentImg.data,
					currentImg.x,
					currentImg.y,
					currentImg.width,
					currentImg.height);
		loadPixels();
	}

	function resetImageTool(){
		// Reset tool state to avoid duplication of image when reselected
		currentImg = {};
		p5image = undefined;
		newImgLoaded = false;
		placeMode = false;
		resizingNow = false;
		movingNow = false;
		imgDownscaled = false;
	}

	this.unselectTool = function() {
		// If the user forgot to press finish, place the image for them
		if(placeMode === true){
			placeImage();
		}
		resetImageTool();
	}

	this.populateOptions = function() {
		// Generate HTML elements: file uploader, image preview, finish button
		let importDiv = createDiv("<h4>Select an image</h4>");
		importDiv.id("import-div");
		importDiv.class("option-block");
		importDiv.parent("left-options");
		// Create import button and add event listener.
		// The function inside runs once a file has been chosen by the user.
		let importButton = createFileInput(function(file){
			select("#middle-2-options").html("");
			p5image = loadImage(file.data);
			previewImg = createImg(file.data, "");
			previewImg.parent("right-options");
			previewImg.id("prev");
			// Reset downscaling flag so next image can be downscaled, this only
			// changes anything if a downscale has happened before.
			imgDownscaled = false;
			// Set flag to tell the tool's draw method that the image is ready
			newImgLoaded = true;
		});
		importButton.id("importButton");
		importButton.parent("import-div");
		importButton.attribute("accept","image/x-png,image/gif,image/jpeg");
		importButton.attribute("type","file");
		// Create finish button and add event listener
		let finishButton = createButton("Finish");
		finishButton.parent("import-div");
		finishButton.id("finish-button");
		finishButton.mousePressed(function(){
			if(placeMode === true){
				placeImage();
				resetImageTool();
			}
		});
	}
}
