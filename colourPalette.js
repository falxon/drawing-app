// Displays and handles the colour palette.
function ColourPalette() {
	// A list of web colour strings
	this.colours = [
        "black", "silver", "gray", "white", "maroon", "crimson", "red",
				"coral", "orange", "yellow", "olive", "greenyellow", "lime",
				"green",  "forestgreen", "teal", "aqua", "darkturquoise",
				"navy", "blue", "darkslateblue", "indigo", "purple", "blueviolet",
				"fuchsia", "deeppink", "pink", "honeydew"
	];
	// Make the start colour be black
	this.selectedColour = "black";

	var self = this;

	var colourClick = function() {
		// Remove the old border
		var current = select("#" + self.selectedColour + "Swatch");
		current.style("border", "0");

		// Get the new colour from the id of the clicked element
		var c = this.id().split("Swatch")[0];

		// Set the selected colour and fill and stroke
		self.selectedColour = c;
		fill(c);
		stroke(c);

		// Add a new border to the selected colour
		this.style("border", "2px solid #8400ff");
		this.style("border-radius", "3px");
	};

	// Load in the colours
	this.loadColours = function() {
		// Set the fill and stroke properties to be black at the start of the programme
		// running
		fill(this.colours[0]);
		stroke(this.colours[0]);

		// For each colour create a new div in the html for the colourSwatches
		for (var i = 0; i < this.colours.length; i++) {
			var colourID = this.colours[i] + "Swatch";

			// Add the swatch to the palette and set its background
			// colour to be the colour value.
			var colourSwatch = createDiv();
			colourSwatch.class("colourSwatches");

			colourSwatch.id(colourID);

			select(".colourPalette").child(colourSwatch);
			select("#" + colourID).style("background-color", this.colours[i]);
			colourSwatch.mouseClicked(colourClick);
		}

		select(".colourSwatches").style("border", "2px solid #8400ff");
		select(".colourSwatches").style("border-radius", "3px");
	};
	// Call the loadColours function now it is declared
	this.loadColours();
}
